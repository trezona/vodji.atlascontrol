﻿using System.IO;
using Vodji.Atlas.AtlasCMD.ShooterGame;
using Vodji.Atlas.AtlasCMD.ShooterGame.Models;
using Vodji.Atlas.Configurations;

namespace Vodji.Atlas.AtlasCMD
{
    public class GameConfig
    {
        public static AppConfig AppConfig { get; set; }

        // ServerGrid
        public static ServerGrid ServerGrid { get; set; }
        public static string WorldAtlasId
        {
            get => ServerGrid.WorldAtlasId;
            set => ServerGrid.WorldAtlasId = value;
        }
        public static string WorldAtlasPassword
        {
            get => ServerGrid.WorldAtlasPassword;
            set => ServerGrid.WorldAtlasPassword = value;
        }
        public static string WorldFriendlyName
        {
            get => ServerGrid.WorldFriendlyName;
            set => ServerGrid.WorldFriendlyName = value;
        }

        public static string GetInstallDir() => Path.GetFullPath((AppConfig.GameInstalled ?? INSTALL_DIR) + "/");


        public static int GetPort(Server server, PortMode portMode = PortMode.None)
        {
            return portMode switch
            {
                PortMode.QueryPort      => AppConfig.QueryPort      + GetPort(server),
                PortMode.GamePort       => AppConfig.GamePort       + GetPort(server),
                PortMode.RconPort       => AppConfig.RconPort       + GetPort(server),
                PortMode.SeamlessPort   => AppConfig.SeamlessPort   + GetPort(server),
                _ => ServerGrid.totalGridsY * server.gridX + server.gridY * 2
            };
        }

        public const string INSTALL_DIR = "AtlasCMD";
        public const string SERVER_EXE = @"ShooterGame\Binaries\Win64\ShooterGameServer.exe";
        public const string SERVER_GRID = @"ShooterGame\ServerGrid.json";
        public const string SERVER_ONLY = @"ShooterGame\ServerGrid.ServerOnly.json";
    }

    public enum PortMode
    {
        None,
        QueryPort,
        GamePort,
        RconPort,
        SeamlessPort
    }
}
