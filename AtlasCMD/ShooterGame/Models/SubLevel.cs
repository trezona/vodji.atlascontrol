﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class SubLevel
    {
        public string name { get; set; }
        public float additionalTranslationX { get; set; }
        public float additionalTranslationY { get; set; }
        public float additionalTranslationZ { get; set; }
        public float additionalRotationPitch { get; set; }
        public float additionalRotationYaw { get; set; }
        public float additionalRotationRoll { get; set; }
        public int id { get; set; }
        public int landscapeMaterialOverride { get; set; }
    }
}
