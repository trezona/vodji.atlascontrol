﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class TribeLogConfig : TravelDataConfig
    {
        public int MaxRedisEntries { get; set; } = 1000;
    }
}
