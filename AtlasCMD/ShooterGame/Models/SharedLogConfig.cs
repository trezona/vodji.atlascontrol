﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class SharedLogConfig : TravelDataConfig
    {
        public int FetchRateSec             { get; set; } = 60;
        public int SnapshotCleanupSec       { get; set; } = 900;
        public int SnapshotRateSec          { get; set; } = 1800;
        public int SnapshotExpirationHours  { get; set; } = 48;
    }
}
