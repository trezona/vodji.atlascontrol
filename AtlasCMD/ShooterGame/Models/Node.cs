﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class Node
    {
        public float controlPointsDistance { get; set; }
        public float worldX { get; set; }
        public float worldY { get; set; }
        public float rotation { get; set; }

        //public Rectangle GetRect(ServerGrid serverGrid)
        //{
        //    if (serverGrid == null) return new();

        //    float relativeX = serverGrid.GetBezierNodeSize() * serverGrid.coordsScaling;
        //    float relativeY = relativeX;

        //    return new Rectangle((int)Math.Round(worldX * serverGrid.coordsScaling - relativeX / 2f), (int)Math.Round(worldY * serverGrid.coordsScaling - relativeY / 2f), (int)Math.Round(relativeX), (int)Math.Round(relativeY));
        //}
        //public bool ContainsPoint(Point p)
        //{
        //    Rectangle Rect = GetRect(ServerGrid.Options.currentProject);

        //    PointF rotatedP = Matematics.RotatePointAround(p, new PointF(Rect.Left + Rect.Width / 2.0f, Rect.Top + Rect.Height / 2.0f), -rotation);
        //    p.X = (int)rotatedP.X;
        //    p.Y = (int)rotatedP.Y;

        //    return Rect.Contains(p);
        //}
        //public PointF GetNextControlPoint()
        //{
        //    PointF controlPoint = new PointF(worldX + controlPointsDistance, worldY);
        //    return Matematics.RotatePointAround(controlPoint, new PointF(worldX, worldY), rotation);
        //}
        //public PointF GetPrevControlPoint()
        //{
        //    PointF controlPoint = new PointF(worldX - controlPointsDistance, worldY);
        //    return Matematics.RotatePointAround(controlPoint, new PointF(worldX, worldY), rotation);
        //}
    }
}
