﻿using Newtonsoft.Json;

namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class DiscoZone
    {
        public string name { get; set; }
        public float sizeX { get; set; }
        public float sizeY { get; set; }
        public float sizeZ { get; set; }
        public int id { get; set; }
        public float xp { get; set; }
        public bool bIsManuallyPlaced { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ManualVolumeName { get; set; }
        public int explorerNoteIndex { get; set; }
        public bool allowSea { get; set; }

        public float worldX { get; set; }
        public float worldY { get; set; }
        public float rotation { get; set; }

        //public Rectangle GetRect(ServerGrid serverGrid)
        //{
        //    if (serverGrid == null) return new Rectangle();

        //    float relativeX = sizeX * serverGrid.coordsScaling;
        //    float relativeY = sizeY * serverGrid.coordsScaling;

        //    return new Rectangle((int)Math.Round(worldX * serverGrid.coordsScaling - relativeX / 2f), (int)Math.Round(worldY * serverGrid.coordsScaling - relativeY / 2f), (int)Math.Round(relativeX), (int)Math.Round(relativeY));
        //}
        //public bool ContainsPoint(Point p)
        //{
        //    Rectangle Rect = GetRect(ServerGrid.Options.currentProject);

        //    PointF rotatedP = Matematics.RotatePointAround(p, new PointF(Rect.Left + Rect.Width / 2.0f, Rect.Top + Rect.Height / 2.0f), -rotation);
        //    p.X = (int)rotatedP.X;
        //    p.Y = (int)rotatedP.Y;

        //    return Rect.Contains(p);
        //}
    }
}
