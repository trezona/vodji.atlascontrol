﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class DatabaseConnections
    {
        public DatabaseConnections() {}
        public DatabaseConnections(string name, string url, int port, string password)
        {
            Name = name;
            URL = url;
            Port = port;
            Password = password;
        }

        public string Name { get; set; }
        public string URL { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
    }
}
