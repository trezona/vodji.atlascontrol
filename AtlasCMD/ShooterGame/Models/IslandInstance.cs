﻿using System.Collections.Generic;
using System.Text.Json;
using Newtonsoft.Json;

namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class IslandInstance
    {
        public string name { get; set; }
        public int id { get; set; }
        public Dictionary<string, string> spawnerOverrides { get; set; }
        public float minTreasureQuality { get; set; }
        public float maxTreasureQuality { get; set; }
        public bool useNpcVolumesForTreasures { get; set; }
        public bool useLevelBoundsForTreasures { get; set; }
        public bool prioritizeVolumesForTreasures { get; set; }
        public int islandPoints { get; set; }
        public string islandTreasureBottleSupplyCrateOverrides { get; set; }
        public float islandWidth { get; set; }
        public float islandHeight { get; set; }

        public float worldX { get; set; }
        public float worldY { get; set; }
        public float rotation { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int spawnPointRegionOverride { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public float instanceTreasureQualityMultiplier { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public float instanceTreasureQualityAddition { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string IslandInstanceCustomDatas1 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string IslandInstanceCustomDatas2 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string IslandInstanceClientCustomDatas1 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string IslandInstanceClientCustomDatas2 { get; set; }
        // ??
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public float finalNPCLevelMultiplier { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int finalNPCLevelOffset { get; set; }

        public string[] treasureMapSpawnPoints { get; set; }
        public string[] wildPirateCampSpawnPoints { get; set; }
        public float singleSpawnPointX { get; set; }
        public float singleSpawnPointY { get; set; }
        public float singleSpawnPointZ { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
        public float maxIslandClaimFlagZ { get; set; }

        // private Image _cachedImg;
        // public Image GetImage(bool optimized = false)
        // {
        //     if (_cachedImg == null)
        //     {
        //         if (File.Exists(imagePath))
        //             cachedImg = Image.FromFile(imagePath);
        //         else
        //         {
        //             MessageBox.Show("Could not find image (Setting to blank image):" + imagePath, "Missing Image Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //             Bitmap bmp = new Bitmap(78, 78);
        //             cachedImg = Image.FromHbitmap(bmp.GetHbitmap());
        //         }
        //     }

        //     if(optimized)
        //     {
        //         if (cachedOptimizedImg == null)
        //         {
        //             if (File.Exists(imagePath))
        //             {
        //                 cachedOptimizedImg = Image.FromFile(imagePath);
        //                 Bitmap bmp = new Bitmap(cachedOptimizedImg, cachedOptimizedImg.Width / 16, cachedOptimizedImg.Height / 16);
        //                 cachedOptimizedImg = (Image)bmp;
        //             }
        //         }

        //         if (cachedOptimizedImg != null)
        //             return cachedOptimizedImg;
        //     }

        //     return cachedImg;
        // }


    }
}
