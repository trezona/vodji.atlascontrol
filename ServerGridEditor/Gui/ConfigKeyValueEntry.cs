﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vodji.Atlas.ServerGridEditor.Gui
{
    public class ConfigKeyValueEntry
    {
        public ConfigKeyValueEntry(string InKey, string InValue)
        {
            Key = InKey;
            Value = InValue;
        }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
