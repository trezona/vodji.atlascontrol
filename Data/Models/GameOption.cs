using System;
namespace Vodji.Atlas.Data.Models
{
    public enum GameSection
    {
        ShooterGameMode,
        ServerSettings
    }

    public class GameOption
    {
        public Guid GameOptionId { get; set; }
        public GameSection Section { get; set; }
        public string OptionName { get; set; }
        public string OptionValue { get; init; }
        public string OptionInfo { get; set; }

        public string GetSectionName(GameSection gameSection)
        {
            return gameSection switch
            {
                GameSection.ServerSettings => "[ServerSettings]",
                GameSection.ShooterGameMode => "[/Script/ShooterGame.ShooterGameMode]",
                _ => "[/Script/ShooterGame.ShooterGameMode]"
            };
        }

        /*public static void WriteDefault(SQLConnect sqlConnect)
        {
            sqlConnect.GameOptions.AddRange(new GameOption[] 
            {
                new GameOption { OptionName = "bUseSettlementClaims", OptionValue = true }
            });
        }*/
    }
}