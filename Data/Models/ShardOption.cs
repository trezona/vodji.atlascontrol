﻿using System;

namespace Vodji.Atlas.Data.Models
{
    public class ShardOption
    {
        public Guid ShardOptionId { get; set; }
        public Guid ShardId { get; set; }

        public string FileName { get; set; }
        public string FileSection { get; set; }
        public string NameOption { get; set; }
        public string ValueOption { get; set; }
    }
}
